import { WorkflowRunner, PrebuiltRunner } from '@itential-tools/iap-cypress-testing-library/testRunner/testRunners';

function initializeWorkflowRunner(workflow, importWorkflow, isStub, stubTasks) {
  let workflowRunner = new WorkflowRunner(workflow.name);
  let getDeviceDetailsWorkflowRunner = new WorkflowRunner(workflow.name);

  if (importWorkflow) {
    // cancel all running jobs for workflow
    workflowRunner.job.cancelAllJobs();
    getDeviceDetailsWorkflowRunner.job.cancelAllJobs();

    workflowRunner.deleteWorkflow.allCopies({
      failOnStatusCode: false
    });

    /* Check if Stub flag is enabled */
    if (isStub) {
      stubTasks.forEach(stubTask => {
        workflow = workflowRunner.stub.task({
          stub: stubTask,
          workflow: workflow,
        });
      })
    }
    workflowRunner.importWorkflow.single({
      workflow,
      failOnStatusCode: false
    });
  }

  /* Verify workflow */
  workflowRunner.verifyWorkflow.exists();
  workflowRunner.verifyWorkflow.hasNoDuplicates({});
  // workflowRunner.verifyWorkflow.dependenciesOnline();
  return workflowRunner;
}

describe('IAG Get Device Details Tests', function () {
  /* Workflow Variables */
  let getDevicesWorkflow;
  let getDeviceDetailsWorkflow;
  /* Stub Variables */
  let getAnsibleDeviceDetailsStub;
  let getHTTPRequestDetailsStub;
  let successfulAnsibleAndHTTPRequestStub;
  let nonexistentAnsibleAndHTTPRequestStub;
  let invalidInventoryTypeStub;
  let adapterDownStub;

  before(function () {
    /* Import Workflows */
    cy.fixture('../../../bundles/workflows/IAGGDD: Get Details For Devices.json').then((data) => {
      getDevicesWorkflow = data;
    });
    cy.fixture('../../../bundles/workflows/IAGGDD: Get Device Details.json').then((data) => {
      getDeviceDetailsWorkflow = data;
    });

    /* Import Stubs */
    cy.fixture('stubs/getAnsibleDeviceDetails.json').then((data) => {
      getAnsibleDeviceDetailsStub = data;
    });
    cy.fixture('stubs/getHTTPRequestDetails.json').then((data) => {
      getHTTPRequestDetailsStub = data;
    });
    cy.fixture('stubs/successfulAnsibleAndHTTPRequest.json').then((data) => {
      successfulAnsibleAndHTTPRequestStub = data;
    });
    cy.fixture('stubs/nonexistentAnsibleAndHTTPRequest.json').then((data) => {
      nonexistentAnsibleAndHTTPRequestStub = data;
    });
    cy.fixture('stubs/ansibleAndHTTPRequestInvalidInventoryType.json').then((data) => {
      invalidInventoryTypeStub = data;
    });
    cy.fixture('stubs/adapterDown.json').then((data) => {
      adapterDownStub = data;
    });

    /* Import prebuilt as fixture */
    cy.fixture('../../../artifact.json').then((data) => {
      /* Using prebuilt runner to ensure prebuilt exists in the IAP environment, this is done within the before all hook */
      let prebuiltRunner = new PrebuiltRunner(data);
      /* Delete existing prebuilt, if any, then reimport */
      prebuiltRunner.deletePrebuilt.single({ failOnStatusCode: false });
      prebuiltRunner.importPrebuilt.single({});
      prebuiltRunner.verifyPrebuilt.exists();
    });
  });

  describe('Testing IAG Get Device Details', function () {

    // 1. Should fail to get details for Ansible device and HTTP Request when unable to connect to adapter
    it("1. Should fail to get details for Ansible device and HTTP Request when unable to connect to adapter", function () {
      const importGetDevicesWorkflow = false;
      const isGetDevicesStub = false;
      const workflowRunner =  initializeWorkflowRunner(getDevicesWorkflow, importGetDevicesWorkflow, isGetDevicesStub, []);
      const input = {
        "variables": {
          "formData": {
            "device": [
              {
                "device_name": "ansibleDevice",
                "iag_adapter_instance": "DSUP IAG-2021.2",
                "inventory_type": "ansible"
              },
              {
                "device_name": "httpRequestTest",
                "iag_adapter_instance": "DSUP IAG-2021.2",
                "inventory_type": "http_requests"
              }
            ],
            "options": {
              "verbose": false
            }
          }
        },
        "description": "1. Should fail to get details for Ansible device and HTTP Request when unable to connect to adapter"
      };
      workflowRunner.job.startAndReturnResultsWhenComplete({
        options: input,
        retryTime: 2000,
      }).then((jobVariableResults) => {
        expect(jobVariableResults['tasks']['945a']['variables']['outgoing']['value']['details'][0]['status']).to.eql('FAILED');
        expect(jobVariableResults['tasks']['945a']['variables']['outgoing']['value']['details'][0]['message']).to.eql('Unable to get device details');
        expect(jobVariableResults['tasks']['945a']['variables']['outgoing']['value']['details'][0]['errors'][0]['message']).to.eql('Unable to get device details');
        expect(jobVariableResults['tasks']['945a']['variables']['outgoing']['value']['details'][1]['status']).to.eql('FAILED');
        expect(jobVariableResults['tasks']['945a']['variables']['outgoing']['value']['details'][1]['message']).to.eql('Unable to get device details');
        expect(jobVariableResults['tasks']['945a']['variables']['outgoing']['value']['details'][1]['errors'][0]['message']).to.eql('Unable to get device');
        expect(jobVariableResults['tasks']['945a']['variables']['outgoing']['value']['number_success']).to.eql(0);
        expect(jobVariableResults['tasks']['945a']['variables']['outgoing']['value']['number_failure']).to.eql(2);
        expect(jobVariableResults['tasks']['945a']['variables']['outgoing']['value']['message']).to.eql('Successfully got device details for 0 device(s), Failed to get device details for 2 device(s).');
        expect(jobVariableResults['tasks']['945a']['variables']['outgoing']['value']['status']).to.eql('FAILED');
      });
    });

    // 2. Should successfully get details for Ansible device.
    it("2. Should successfully get details for Ansible device.", function () {
      const importGetDevicesWorkflow = false;
      const isGetDevicesStub = false;
      const importGetDeviceDetailsWorkflow = true;
      const isGetDeviceDetailsStub = true;
      const workflowRunner = initializeWorkflowRunner(getDevicesWorkflow, importGetDevicesWorkflow, isGetDevicesStub, []);
      const getDeviceDetailsWorkflowRunner = initializeWorkflowRunner(getDeviceDetailsWorkflow, importGetDeviceDetailsWorkflow, isGetDeviceDetailsStub, getAnsibleDeviceDetailsStub.stubTasks);
      const input = {
        "variables": {
          "formData": {
            "device": [
              {
                "device_name": "ansibleDevice",
                "iag_adapter_instance": "DSUP IAG-2021.2",
                "inventory_type": "ansible"
              }
            ],
            "options": {
              "verbose": false
            }
          }
        },
        "description": "2. Should successfully get details for Ansible device"
      };
      workflowRunner.job.startAndReturnResultsWhenComplete({
        options: input,
        retryTime: 2000,
      }).then((jobVariableResults) => {
        expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['details'][0]['status']).to.eql('SUCCESS');
        expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['details'][0]['message']).to.eql('Device details found');
        expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['number_success']).to.eql(1);
        expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['number_failure']).to.eql(0);
        expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['message']).to.eql('Successfully got device details for 1 device(s), Failed to get device details for 0 device(s).');
        expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['status']).to.eql('SUCCESS');
      });
      getDeviceDetailsWorkflowRunner.deleteWorkflow.allCopies({
        failOnStatusCode: false,
      });
      getDeviceDetailsWorkflowRunner.importWorkflow.single({ workflow: getDeviceDetailsWorkflow });
      getDeviceDetailsWorkflowRunner.verifyWorkflow.exists();
      getDeviceDetailsWorkflowRunner.verifyWorkflow.hasNoDuplicates({});
    });

    // 3. Should successfully get details for HTTP Request
    it("3. Should successfully get details for HTTP Request", function () {
      const importGetDevicesWorkflow = false;
      const isGetDevicesStub = false;
      const importGetDeviceDetailsWorkflow = true;
      const isGetDeviceDetailsStub = true;
      const workflowRunner = initializeWorkflowRunner(getDevicesWorkflow, importGetDevicesWorkflow, isGetDevicesStub, []);
      const getDeviceDetailsWorkflowRunner = initializeWorkflowRunner(getDeviceDetailsWorkflow, importGetDeviceDetailsWorkflow, isGetDeviceDetailsStub, getHTTPRequestDetailsStub.stubTasks);
      const input = {
        "variables": {
          "formData": {
            "device": [
              {
                "device_name": "httpRequestDevice",
                "iag_adapter_instance": "DSUP IAG-2021.2",
                "inventory_type": "http_requests"
              }
            ],
            "options": {
              "verbose": false
            }
          }
        },
        "description": "3. Should successfully get details for HTTP Request"
      };
      workflowRunner.job.startAndReturnResultsWhenComplete({
        options: input,
        retryTime: 2000,
      }).then((jobVariableResults) => {
        expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['details'][0]['status']).to.eql('SUCCESS');
        expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['details'][0]['message']).to.eql('Device details found');
        expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['number_success']).to.eql(1);
        expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['number_failure']).to.eql(0);
        expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['message']).to.eql('Successfully got device details for 1 device(s), Failed to get device details for 0 device(s).');
        expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['status']).to.eql('SUCCESS');
      });
      getDeviceDetailsWorkflowRunner.deleteWorkflow.allCopies({
        failOnStatusCode: false,
      });
      getDeviceDetailsWorkflowRunner.importWorkflow.single({ workflow: getDeviceDetailsWorkflow });
      getDeviceDetailsWorkflowRunner.verifyWorkflow.exists();
      getDeviceDetailsWorkflowRunner.verifyWorkflow.hasNoDuplicates({});
    });

    // 4. Should successfully get details for Ansible device and HTTP Request.
    it("4. Should successfully get details for Ansible device.", function () {
      const importGetDevicesWorkflow = true;
      const isGetDevicesStub = true;
      const workflowRunner = initializeWorkflowRunner(getDevicesWorkflow, importGetDevicesWorkflow, isGetDevicesStub, successfulAnsibleAndHTTPRequestStub.stubTasks);
      const input = {
        "variables": {
          "formData": {
            "device": [
              {
                "device_name": "ansibleTest",
                "iag_adapter_instance": "DSUP IAG-2021.2",
                "inventory_type": "ansible"
              },
              {
                "device_name": "httpTest",
                "iag_adapter_instance": "DSUP IAG-2021.2",
                "inventory_type": "http_requests"
              }
            ],
            "options": {
              "verbose": false
            }
          }
        },
        "description": "4. Should successfully get details for Ansible device and HTTP Request"
      };
      workflowRunner.job.startAndReturnResultsWhenComplete({
        options: input,
        retryTime: 2000,
      }).then((jobVariableResults) => {
        expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['details'][0]['status']).to.eql('SUCCESS');
        expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['details'][0]['message']).to.eql('Device details found');
        expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['details'][1]['status']).to.eql('SUCCESS');
        expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['details'][1]['message']).to.eql('Device details found');
        expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['number_success']).to.eql(2);
        expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['number_failure']).to.eql(0);
        expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['message']).to.eql('Successfully got device details for 2 device(s), Failed to get device details for 0 device(s).');
        expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['status']).to.eql('SUCCESS');
      });
      workflowRunner.deleteWorkflow.allCopies({
        failOnStatusCode: false,
      });
      workflowRunner.importWorkflow.single({ workflow: getDevicesWorkflow });
      workflowRunner.verifyWorkflow.exists();
      workflowRunner.verifyWorkflow.hasNoDuplicates({});
    });

    // 5. Should fail to get details for Ansible device due to non-existent devices.
    it("5. Should fail to get details for Ansible device due to non-existent devices", function () {
      const importGetDevicesWorkflow = true;
      const isGetDevicesStub = true;
      const workflowRunner = initializeWorkflowRunner(getDevicesWorkflow, importGetDevicesWorkflow, isGetDevicesStub, nonexistentAnsibleAndHTTPRequestStub.stubTasks);
      const input = {
        "variables": {
          "formData": {
            "device": [
              {
                "device_name": "nonExistentAnsible",
                "iag_adapter_instance": "DSUP IAG-2021.2",
                "inventory_type": "ansible"
              },
              {
                "device_name": "nonExistentHTTPRequest",
                "iag_adapter_instance": "DSUP IAG-2021.2",
                "inventory_type": "http_requests"
              }
            ],
            "options": {
              "verbose": false
            }
          }
        },
        "description": "5. Should fail to get details for Ansible device due to non-existent devices"
      };
      workflowRunner.job.startAndReturnResultsWhenComplete({
        options: input,
        retryTime: 2000,
      }).then((jobVariableResults) => {
        expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['details'][0]['status']).to.eql('FAILED');
        expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['details'][0]['message']).to.eql('Unable to get device details');
        expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['details'][1]['status']).to.eql('FAILED');
        expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['details'][1]['message']).to.eql('Unable to get device details');
        expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['number_success']).to.eql(0);
        expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['number_failure']).to.eql(2);
        expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['message']).to.eql('Successfully got device details for 0 device(s), Failed to get device details for 2 device(s).');
        expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['status']).to.eql('FAILED');
      });
      workflowRunner.deleteWorkflow.allCopies({
        failOnStatusCode: false,
      });
      workflowRunner.importWorkflow.single({ workflow: getDevicesWorkflow });
      workflowRunner.verifyWorkflow.exists();
      workflowRunner.verifyWorkflow.hasNoDuplicates({});
    });

    // 6. Should fail to get details for Ansible device and HTTP Request due to invalid inventory type
    it("6. Should fail to get details for Ansible device and HTTP Request due to invalid inventory type", function () {
      const importGetDevicesWorkflow = true;
      const isGetDevicesStub = true;
      const workflowRunner = initializeWorkflowRunner(getDevicesWorkflow, importGetDevicesWorkflow, isGetDevicesStub, invalidInventoryTypeStub.stubTasks);
      const input = {
        "variables": {
          "formData": {
            "device": [
              {
                "device_name": "ansibleDevice",
                "iag_adapter_instance": "DSUP IAG-2021.2",
                "inventory_type": "invalidInventoryType"
              },
              {
                "device_name": "httpRequestDevice",
                "iag_adapter_instance": "DSUP IAG-2021.2",
                "inventory_type": "invalidInventoryType"
              }
            ],
            "options": {
              "verbose": false
            }
          }
        },
        "description": "6. Should fail to get details for Ansible device and HTTP Request due to invalid inventory type"
      };
      workflowRunner.job.startAndReturnResultsWhenComplete({
        options: input,
        retryTime: 2000,
      }).then((jobVariableResults) => {
        expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['details'][0]['status']).to.eql('FAILED');
        expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['details'][0]['message']).to.eql('Unable to get device details');
        expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['details'][1]['status']).to.eql('FAILED');
        expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['details'][1]['message']).to.eql('Unable to get device details');
        expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['number_success']).to.eql(0);
        expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['number_failure']).to.eql(2);
        expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['message']).to.eql('Successfully got device details for 0 device(s), Failed to get device details for 2 device(s).');
        expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['status']).to.eql('FAILED');
      });
      workflowRunner.deleteWorkflow.allCopies({
        failOnStatusCode: false,
      });
      workflowRunner.importWorkflow.single({ workflow: getDevicesWorkflow });
      workflowRunner.verifyWorkflow.exists();
      workflowRunner.verifyWorkflow.hasNoDuplicates({});
    });

    // 7. Should fail to get details for Ansible device and HTTP Request due to adapter being down
    it("7. Should fail to get details for Ansible device and HTTP Request due to adapter being down", function () {
      const importGetDevicesWorkflow = true;
      const isGetDevicesStub = true;
      const workflowRunner = initializeWorkflowRunner(getDevicesWorkflow, importGetDevicesWorkflow, isGetDevicesStub, adapterDownStub.stubTasks);
      const input = {
        "variables": {
          "formData": {
            "device": [
              {
                "device_name": "ansibleTest",
                "iag_adapter_instance": "DSUP IAG-2021.2",
                "inventory_type": "ansible"
              },
              {
                "device_name": "httpTest",
                "iag_adapter_instance": "DSUP IAG-2021.2",
                "inventory_type": "http_requests"
              }
            ],
            "options": {
              "verbose": false
            }
          }
        },
        "description": "7. Should fail to get details for Ansible device and HTTP Request due to adapter being down"
      };
      workflowRunner.job.startAndReturnResultsWhenComplete({
        options: input,
        retryTime: 2000,
      }).then((jobVariableResults) => {
        expect(jobVariableResults['tasks']['945a']['variables']['outgoing']['value']['details'][0]['status']).to.eql('FAILED');
        expect(jobVariableResults['tasks']['945a']['variables']['outgoing']['value']['details'][0]['message']).to.eql('Unable to get device details');
        expect(jobVariableResults['tasks']['945a']['variables']['outgoing']['value']['details'][0]['errors'][0]['message']).to.eql('Unable to get device details');
        expect(jobVariableResults['tasks']['945a']['variables']['outgoing']['value']['details'][0]['response']['device_details']).to.eql('Cannot forward message to corral:DSUP IAG-2021.2');
        expect(jobVariableResults['tasks']['945a']['variables']['outgoing']['value']['details'][1]['status']).to.eql('FAILED');
        expect(jobVariableResults['tasks']['945a']['variables']['outgoing']['value']['details'][1]['message']).to.eql('Unable to get device details');
        expect(jobVariableResults['tasks']['945a']['variables']['outgoing']['value']['details'][1]['errors'][0]['message']).to.eql('Unable to get device');
        expect(jobVariableResults['tasks']['945a']['variables']['outgoing']['value']['details'][1]['response']['device_details']).to.eql('Cannot forward message to corral:DSUP IAG-2021.2');
        expect(jobVariableResults['tasks']['945a']['variables']['outgoing']['value']['number_success']).to.eql(0);
        expect(jobVariableResults['tasks']['945a']['variables']['outgoing']['value']['number_failure']).to.eql(2);
        expect(jobVariableResults['tasks']['945a']['variables']['outgoing']['value']['message']).to.eql('Successfully got device details for 0 device(s), Failed to get device details for 2 device(s).');
        expect(jobVariableResults['tasks']['945a']['variables']['outgoing']['value']['status']).to.eql('FAILED');
      });
      workflowRunner.deleteWorkflow.allCopies({
        failOnStatusCode: false,
      });
      workflowRunner.importWorkflow.single({ workflow: getDevicesWorkflow });
      workflowRunner.verifyWorkflow.exists();
      workflowRunner.verifyWorkflow.hasNoDuplicates({});
    });
  });
})
