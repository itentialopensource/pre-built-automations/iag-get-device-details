
## 0.0.7 [06-21-2023]

* Merging pre-release/2023.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/iag-get-device-details!8

---

## 0.0.6 [04-26-2023]

* Update master to reflect latest changes

See merge request itentialopensource/pre-built-automations/iag-get-device-details!7

---

## 0.0.6-2021.2 [04-26-2023]

* Update cypress tests to add stubs

See merge request itentialopensource/pre-built-automations/iag-get-device-details!5

---

## 0.0.5-2021.2.1 [02-15-2023]

* Update README.md

See merge request itentialopensource/pre-built-automations/staging/iag-get-device-details!4

---

## 0.0.5-2021.2.0 [02-02-2023]

* Update README.md

See merge request itentialopensource/pre-built-automations/staging/iag-get-device-details!4

---

## 0.0.5 [02-02-2023]

* Bug fixes and performance improvements

See commit 169a831

---

## 0.0.5 [02-01-2023]

* Bug fixes and performance improvements

See commit 832f79c

---

## 0.0.3-2021.2.1 [01-31-2023]

* patch/2023-01-23T11-20-04

See merge request itentialopensource/pre-built-automations/staging/iag-get-device-details!3

---

## 0.0.3-2021.2.0 [01-30-2023]

* patch/2023-01-23T11-20-04

See merge request itentialopensource/pre-built-automations/staging/iag-get-device-details!3

---

## 0.0.3 [01-30-2023]

* patch/2023-01-23T11-20-04

See merge request itentialopensource/pre-built-automations/staging/iag-get-device-details!3

---

## 0.0.2 [01-04-2023]

* Bug fixes and performance improvements

See commit 5b78a70

---

## 0.0.7 [04-21-2022]

* Bug fixes and performance improvements

See commit 3983a14

---

## 0.0.6 [10-15-2020]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.5 [08-03-2020]

* Fixed repository.url in Package.json

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!2

---

## 0.0.4 [07-17-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.3 [07-07-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.2 [06-19-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---\n\n\n
