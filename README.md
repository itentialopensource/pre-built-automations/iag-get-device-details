# IAG Get Device Details

## Table of Contents

- [IAG Get Device Details](#iag-get-device-details)
  - [Table of Contents](#table-of-contents)
  - [Overview](#overview)
  - [Supported IAP Versions](#supported-iap-versions)
  - [Getting Started](#getting-started)
    - [Prerequisites](#prerequisites)
    - [Capabilities](#capabilities)
    - [How to Install](#how-to-install)
    - [Testing](#testing)
  - [Using this Pre-Built](#using-this-prebuilt)
    - [Input Schema](#input-schema)
    - [Output Schema](#output-schema)
  - [Additional Information](#additional-information)

## Overview

The **IAG Get Device Details** pre-built is used to determine if a list of devices are in the inventory of an IAG instance and returns their details. 

Estimated Run Time: < 1 minute for single device

## Supported IAP Versions

Itential pre-builts are built and tested on particular versions of IAP. In addition, pre-builts that work with devices are often dependent on certain orchestration systems (e.g. NSO and IAG). As such, these pre-builts will have dependencies on these other systems. This version of the IAG Get Device Details pre-built has been tested with:

- IAP 2022.1.x

## Getting Started

These instructions will help you get a copy of the pre-built in your local machine for testing in your environment. Reading this section is also helpful for deployments as it provides you with pertinent information on prerequisites and capabilities.

### Prerequisites

Users must satisfy the following prerequisites to install and run this pre-built:

- Itential Automation Platform
  - `^2022.1.x`
- An instantiated IAP IAG adapter

### Capabilities

- Get the current details for a list of devices in IAG.
- Ability to discover multiple device types (Currently only supports HTTP Request and Ansible device types).
- Return device details with authentication details removed.

### How to Install

To install this pre-built:

- Verify that you are running the documented [prerequisites](#prerequisites) in order to install the pre-built.

- Follow the instructions on the Itential Documentation site for [importing a pre-built](https://docs.itential.com/docs/importing-a-prebuilt-2).

### Testing

While Itential tests this pre-built and its capabilities, it is often the case the customer environments offer their own unique circumstances. Therefore, it is our recommendation that you deploy this pre-built into a development/testing environment in which you can test the pre-built.

## Using this Pre-Built

This pre-built can be run as a standalone automation or in a [childJob task](https://docs.itential.com/docs/childjob-1).

When running this pre-built, it depends on being provided proper input so that the device details can be discovered as expected. The input to and possible outputs from this pre-built are described here.

### Input Schema

Example input for an Ansible Device: 
```json
{
  "formData": {
    "device": [
      {
        "device_name": "test_device",
        "iag_adapter_instance": "IAG-2021.2",
        "inventory_type": "ansible"
      }
    ],
    "options": {
      "verbose": true
    }
  }
}
```
Example input for an HTTP Request:
```json
{
  "formData": {
    "device": [
      {
        "device_name": "test_device",
        "iag_adapter_instance": "IAG-2021.2",
        "inventory_type": "http_requests"
      }
    ],
    "options": {
      "verbose": true
    }
  }
}
```


The following table details the property keys of the `formData` input object.
| key                                      | type    | required | description                                             |
|------------------------------------------|---------|----------|---------------------------------------------------------|
| formData.device                          | array   | yes      | list of devices                                         |
| formData.device.deviceName               | string  | yes      | device name                                             |
| formData.device.inventoryType            | string  | yes      | inventory type [ansible or http_requests]               |
| formData.device.iagAdapterInstance       | string  | yes      | adapter IAP instance                          
| formData.options                         | object  | yes      | additional options                                      |
| formData.options.verbose                 | boolean | yes      | verbose                                                 |

### Output Schema

The `ReturnStatus` job variable returned from the run pre-built reports the success or failure of acquiring IAG device details for a device. 

Example output for Ansible Device:

```json
{
  "ReturnStatus": {
    "details": [
      {
        "status": "SUCCESS",
        "device": "test_device",
        "inventory_type": "ansible",
        "message": "Device details found",
        "errors": [],
        "response": {
          "device_details": {
            "name": "test_device",
            "device-type": "network_cli",
            "ipaddress": "1.1.1.1",
            "port": "22",
            "ostype": "cisco-ios",
            "become": "true",
            "become_method": "enable"
          }
        }
      }
    ],
    "errors": [],
    "number_success": 1,
    "number_failure": 0,
    "message": "Sucessfully got device details for 1 device(s), Failed to get device details for 0 device(s).",
    "status": "SUCESS"
  }
}
```
Example output for HTTP Request:

```json
{
  "ReturnStatus": {
    "details": [
      {
        "status": "SUCCESS",
        "device": "test_device",
        "inventory_type": "http_requests",
        "message": "Device details found",
        "errors": [],
        "response": {
          "device_details": {
            "icode": "AD.200",
            "response": {
              "name": "test_device",
              "variables": {
                "base_url": "1.1.1.1:443/mgmt",
                "protocol": "https",
                "endpoint": "/tm/ltm/pool",
                "method": "",
                "params": {},
                "data": {},
                "headers": {},
                "cookies": {},
                "timeout": {},
                "allowRedirects": true,
                "proxies": {},
                "verify": false
              }
            },
            "headers": {
              "server": "gunicorn/20.0.4",
              "date": "Wed, 18 Jan 2023 17:11:44 GMT",
              "connection": "close",
              "content-type": "application/json;",
              "content-length": "332",
              "last-modified": "2023-01-18 12:11:44.586347",
              "cache-control": "no-store, no-cache, must-revalidate, post-check=0, pre-check=0, max-age=0",
              "pragma": "no-cache",
              "expires": "-1",
              "x-frame-options": "DENY",
              "x-xss-protection": "1",
              "x-content-type-options": "nosniff"
            },
            "metrics": {
              "code": 200,
              "timeouts": 0,
              "redirects": 0,
              "retries": 0,
              "tripTime": 38,
              "isThrottling": false,
              "capabilityTime": "194ms"
            }
          }
        }
      }
    ],
    "errors": [],
    "number_success": 1,
    "number_failure": 0,
    "message": "Sucessfully got device details for 1 device(s), Failed to get device details for 0 device(s).",
    "status": "SUCESS"
  }
}
```

Example output for failure getting device details:

```json
{
  "ReturnStatus": {
    "errors": [],
    "details": [
      {
        "status": "FAILED",
        "device": "test_device_non_existent",
        "inventory_type": "ansile",
        "message": "Unable to get device details",
        "errors": [
          {
            "error_details": {
              "icode": "AD.500",
              "IAPerror": {
                "origin": "IAG-2021.2-connectorRest-handleEndResponse",
                "displayString": "Error 404 received on request",
                "recommendation": "Verify the request is accurate via debug logs and postman",
                "code": 404,
                "raw_response": {
                  "status": "success",
                  "code": 404,
                  "headers": {
                    "server": "gunicorn/20.0.4",
                    "date": "Wed, 18 Jan 2023 17:18:40 GMT",
                    "connection": "close",
                    "content-type": "application/json;",
                    "content-length": "52",
                    "last-modified": "2023-01-18 12:18:40.184486",
                    "cache-control": "no-store, no-cache, must-revalidate, post-check=0, pre-check=0, max-age=0",
                    "pragma": "no-cache",
                    "expires": "-1",
                    "x-frame-options": "DENY",
                    "x-xss-protection": "1",
                    "x-content-type-options": "nosniff"
                  },
                  "response": "{\"code\": 404, \"status\": 404, \"message\": \"Not Found\"}",
                  "redirects": 0,
                  "tripTime": "34ms",
                  "reqHdr": {
                    "Content-Type": "application/json",
                    "Accept": "application/json"
                  }
                }
              },
              "metrics": {
                "code": 404,
                "timeouts": 0,
                "redirects": 0,
                "retries": 0,
                "tripTime": 34,
                "isThrottling": false,
                "capabilityTime": "209ms"
              },
              "response": {
                "code": 404,
                "status": 404,
                "message": "Not Found"
              }
            },
            "message": "Unable to get device details"
          }
        ],
        "response": {
          "device_details": {
            "icode": "AD.500",
            "IAPerror": {
              "origin": "IAG-2021.2-connectorRest-handleEndResponse",
              "displayString": "Error 404 received on request",
              "recommendation": "Verify the request is accurate via debug logs and postman",
              "code": 404,
              "raw_response": {
                "status": "success",
                "code": 404,
                "headers": {
                  "server": "gunicorn/20.0.4",
                  "date": "Wed, 18 Jan 2023 17:18:40 GMT",
                  "connection": "close",
                  "content-type": "application/json;",
                  "content-length": "52",
                  "last-modified": "2023-01-18 12:18:40.184486",
                  "cache-control": "no-store, no-cache, must-revalidate, post-check=0, pre-check=0, max-age=0",
                  "pragma": "no-cache",
                  "expires": "-1",
                  "x-frame-options": "DENY",
                  "x-xss-protection": "1",
                  "x-content-type-options": "nosniff"
                },
                "response": "{\"code\": 404, \"status\": 404, \"message\": \"Not Found\"}",
                "redirects": 0,
                "tripTime": "34ms",
                "reqHdr": {
                  "Content-Type": "application/json",
                  "Accept": "application/json"
                }
              }
            },
            "metrics": {
              "code": 404,
              "timeouts": 0,
              "redirects": 0,
              "retries": 0,
              "tripTime": 34,
              "isThrottling": false,
              "capabilityTime": "209ms"
            },
            "response": {
              "code": 404,
              "status": 404,
              "message": "Not Found"
            }
          }
        }
      }
    ],
    "number_success": 0,
    "number_failure": 1,
    "message": "Sucessfully got device details for 0 device(s), Failed to get device details for 1 device(s).",
    "status": "FAILED"
  }
}
```

The following table details the property keys of the `ReturnStatus` object.
|                       key                      |   type  |                         description                        |
|:----------------------------------------------:|:-------:|:----------------------------------------------------------:|
| ReturnStatus.errors                            | array   | list of errors encountered returned from running pre-built |
| ReturnStatus.response.number_success           | integer | number of successfully found device details                |
| ReturnStatus.response.number_failure           | integer | number of device details failed to be found                |
| ReturnStatus.status                            | string  | status of getting list of device details [SUCCESS, FAILED] |
| ReturnStatus.device                            | string  | device name                                                |
| ReturnStatus.inventory_type                    | string  | inventory type [ansible or http_requests]                  |
| ReturnStatus.message                           | string  | description of getting list of device details result       |
| ReturnStatus.details[x].status                 | string  | status of single device result [SUCCESS, FAILED]           |
| ReturnStatus.details[x].device                 | string  | device name                                                |
| ReturnStatus.details[x].inventory_type         | string  | inventory type [ansible or http_requests]                  |
| ReturnStatus.details[x].message                | string  | description of single device result                        |
| ReturnStatus.details[x].errors                 | array   | list of errors encountered onboaring single device         |
| ReturnStatus.details[x].reponse.device_details | object  | response of getting device details for a single device     |

## Additional Information

Please use your Itential Customer Success account if you need support when using this pre-built.
